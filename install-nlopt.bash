#!/bin/bash
# Author: J. Bect, 2014/11
set -e

cd $(dirname $0)
NLOPT_SRC=`pwd`
echo NLOPT_SRC=$NLOPT_SRC

######

if [ "$1" == "" ]
then
    echo "Please provide PREFIX as \$1"
    exit 1
else
    PREFIX=$1
fi

mkdir -p $PREFIX
cd $PREFIX
PREFIX=`pwd`
echo PREFIX=$PREFIX

######

# I'm only interested in the Matlab & Octave interfaces (for now)
OPTIONS="--without-guile --without-python"

NLOPT_VER=`grep AC_INIT ${NLOPT_SRC}/configure.ac |grep -o [0-9]\.[0-9]\.[0-9]`
NLOPT_NAME=nlopt-${NLOPT_VER}

# Installation directory for Octave and Matlab plugins
export M_INSTALL_DIR=${PREFIX}/share/nlopt/${NLOPT_VER}
export MEX_INSTALL_DIR=${PREFIX}/lib/nlopt/${NLOPT_VER}
export OCT_INSTALL_DIR=${MEX_INSTALL_DIR}

cd ${NLOPT_SRC}
./autogen.sh --no-configure
./configure --prefix=${PREFIX} \
  --enable-maintainer-mode --enable-shared $OPTIONS

make
make install

#####

rm -f ${MEX_INSTALL_DIR}/*.m

ln -snf ${PREFIX}/share/nlopt/${NLOPT_VER} ${PREFIX}/share/nlopt/current
ln -snf ${PREFIX}/lib/nlopt/${NLOPT_VER} ${PREFIX}/lib/nlopt/current

# Shared libraries don't need to be executable
chmod 644 ${PREFIX}/lib/libnlopt.*
chmod 644 ${PREFIX}/lib/nlopt/${NLOPT_VER}/nlopt_optimize*

#####

## The following directories must be added to the path in
## in Matlab and Octave (in THIS order):
##
##   addpath ('/usr/local/share/nlopt/current');  # arch-independent FIRST
##   addpath ('/usr/local/lib/nlopt/current');    # arch-dependent
##
## To make these directories available for all users, the following
## system-level startup files must be modified:
##
##   ${PREFIX}/share/octave/site/m/startup/octaverc
##   ${MATLAB}/toolbox/local/matlabrc.m 
